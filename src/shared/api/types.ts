import { LoginInput } from 'shared/types/inputTypes';

export interface PostLoginUser extends LoginInput {
    session_id: string;
}

export interface PostEmailCode {
    code: string;
    session_id: string;
}

export interface RenewalAccessToken {
    session_id: string;
    access_token: string;
}

export interface PostRegistrationUser extends PostLoginUser {
    referral_link?: string;
    language_id: number;
}

export interface PositiveResponseRegistration {
    registration: {
        needCode: boolean;
    };
}

export interface PositiveEmailCode {
    email_code: {
        done: boolean;
        description: Object;
    };
}

export interface NegativeEmailCode {
    email_code: {
        error: 'string';
    };
}

export interface PostAddMarket {
    name: string;
    type: number;
    key: string;
    secret: string;
}

export interface AddMarketResponse {
    create_credentials_api: {
        api_id: number;
    };
}

export interface Market {
    api_id: number;
    name: string;
    type: number;
    secret: string;
}

export interface GetMarketInfoResponse {
    task: string;
    get_user_api_id: Market[];
}

export interface Signal {
    id: number;
    name: string;
    type: string;
    telegram_free_link: string;
    descr: string;
    tariffs_id: number;
    wallets_id: number[];
}
export interface GetSignalsResponse {
    get_showcase: {
        showcases: Signal[];
        task: string;
    };
}
