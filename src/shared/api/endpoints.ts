export const sendEmailEndpoint = '/email';
export const renewalAccessWSTokenEndpoint = '/renewal';
export const loginEndpoint = '/login';
export const registrationEndpoint = '/registration';
export const sendEmailCodeEndpoint = '/email';
export const getMarketsEndpoint = '/get_user_api_id';
export const getShowcase = '/get_showcase';
