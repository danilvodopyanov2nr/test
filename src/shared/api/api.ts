import { $api } from 'shared/api';
import {
    getMarketsEndpoint,
    getShowcase,
    loginEndpoint,
    registrationEndpoint,
    renewalAccessWSTokenEndpoint,
    sendEmailCodeEndpoint,
} from 'shared/api/endpoints';
import {
    PostLoginUser,
    PostRegistrationUser,
    PositiveResponseRegistration,
    PostEmailCode,
    PositiveEmailCode,
    RenewalAccessToken,
    GetMarketInfoResponse,
    NegativeEmailCode,
    GetSignalsResponse,
} from 'shared/api/types';

import { UserApiResponse } from 'shared/types/user';

export const loginUser = async (user: PostLoginUser) => {
    const response = await $api.post<UserApiResponse>(loginEndpoint, {
        authorization: user,
    });
    return response.data;
};

export const registrationUser = async (user: PostRegistrationUser) => {
    const response = await $api.post<PositiveResponseRegistration>(registrationEndpoint, {
        registration: user,
    });
    return response.data;
};

export const getMarkets = async (access_token: string) => {
    const response = await $api.post<GetMarketInfoResponse>(getMarketsEndpoint, null, {
        params: {
            access_token,
        },
    });
    return response.data;
};

export const getSignals = async (access_token: string) => {
    const response = await $api.post<GetSignalsResponse>(getShowcase, null, {
        params: {
            access_token,
        },
    });
    return response.data;
};

export const sendEmailCode = async (emailCode: PostEmailCode) => {
    const response = await $api.post<PositiveEmailCode | NegativeEmailCode>(
        sendEmailCodeEndpoint,
        {
            email_code: emailCode,
        }
    );
    return response.data;
};

export const renewalAccessToken = async ({
    session_id,
    access_token,
}: RenewalAccessToken) => {
    await $api.post(renewalAccessWSTokenEndpoint, {
        renewal_token: {
            session_id,
            access_token,
        },
    });
};
