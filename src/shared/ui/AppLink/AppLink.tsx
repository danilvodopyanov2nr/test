import { styled } from '@mui/material';
import { FC } from 'react';
import { Link, LinkProps } from 'react-router-dom';

export const AppLink: FC<LinkProps> = (props) => {
    const { children, to, ...otherProps } = props;

    return (
        <LinkButton to={to} {...otherProps}>
            {children}
        </LinkButton>
    );
};

const LinkButton = styled(Link)({
    color: 'inherit',
    textDecoration: 'none',
    textTransform: 'none',
});
