import { FC, ReactNode } from 'react';

interface DropdownProps {
    label: string;
    children: ReactNode;
}

export const Dropdown: FC<DropdownProps> = ({ label, children }) => {
    return (
        <div className="dropdown dropdown-end">
            <label tabIndex={0} className="btn btn-ghost rounded-btn">
                {label}
            </label>
            {children}
        </div>
    );
};
