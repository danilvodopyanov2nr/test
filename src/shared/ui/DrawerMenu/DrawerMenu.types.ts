export interface MenuItem {
    name: string;
    path: string;
}

export interface DrawerMenuProps {
    isOpen: boolean;
    setIsOpen: (isOpen: boolean) => void;
    drawerWidth: number;
    appBarWidth: number;
    menuItems: MenuItem[];
}
