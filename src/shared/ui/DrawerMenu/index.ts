import DrawerMenu from 'shared/ui/DrawerMenu/DrawerMenu';
export type { MenuItem, DrawerMenuProps } from './DrawerMenu.types';
export { DrawerMenu };
