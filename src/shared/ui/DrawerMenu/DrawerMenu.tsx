import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import CssBaseline from '@mui/material/CssBaseline';

import Divider from '@mui/material/Divider';

import LogoIcon from 'shared/assets/icons/logo.svg';

import { DrawerMenuProps } from 'shared/ui/DrawerMenu/DrawerMenu.types';
import { MenuItem } from '@mui/material';
import { AppLink } from 'shared/ui/AppLink';

const DrawerHeader = styled('div')(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 1),

    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
}));

export default function DrawerMenu({
    isOpen,
    setIsOpen,
    drawerWidth,
    appBarWidth,
    menuItems,
}: DrawerMenuProps) {
    const handleDrawerClose = () => {
        setIsOpen(false);
    };

    return (
        <Box sx={{ display: 'flex' }}>
            <CssBaseline />

            <Drawer
                sx={{
                    width: appBarWidth + drawerWidth,

                    '& .MuiDrawer-paper': {
                        boxShadow:
                            '0px 1px 3px rgba(0, 0, 0, 0.12), 0px 1px 1px rgba(0, 0, 0, 0.14), 0px 2px 1px -1px rgba(0, 0, 0, 0.2)',
                        marginLeft: appBarWidth - 3 + 'px',
                        width: appBarWidth + drawerWidth,

                        borderLeft: '1px solid rgba(0, 0, 0, 0.12)',
                    },
                    transform: 'none',
                }}
                variant="persistent"
                anchor="left"
                open={isOpen}
                ModalProps={{ onBackdropClick: handleDrawerClose }}
            >
                <DrawerHeader sx={{ display: 'flex', justifyContent: 'center' }}>
                    <LogoIcon />
                </DrawerHeader>
                <Divider />
                <Box
                    sx={{
                        display: 'flex',
                        flexDirection: 'column',
                    }}
                >
                    {menuItems.map((menuItem) => (
                        <MenuItem>
                            <StyledMenuItem
                                style={{
                                    color: 'rgba(0, 0, 0, 0.87);',
                                    fontSize: '18px',
                                    marginTop: '8px',
                                }}
                                to={menuItem.path}
                            >
                                {menuItem.name}
                            </StyledMenuItem>
                        </MenuItem>
                    ))}
                </Box>
            </Drawer>
        </Box>
    );
}
const StyledMenuItem = styled(AppLink)({
    color: 'inherit',
    textDecoration: 'none',
    textTransform: 'none',
    '&:hover': {},
});
