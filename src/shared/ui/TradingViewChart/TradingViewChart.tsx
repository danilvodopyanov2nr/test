import { AdvancedRealTimeChart } from 'react-ts-tradingview-widgets';

const TradingViewChart = () => {
    return <AdvancedRealTimeChart theme="light"></AdvancedRealTimeChart>;
};

export default TradingViewChart;
