import styled from '@emotion/styled';
import { Box, Divider, Paper, PaperProps, Typography } from '@mui/material';
import { FC, PropsWithChildren, ReactElement } from 'react';

interface ContentWrapperProps {
    header?: ReactElement;
    footer?: ReactElement;
    label?: string;
    fullWidth?: boolean;
    fullHeight?: boolean;
}

export const ContentWrapper: FC<PropsWithChildren<ContentWrapperProps>> = ({
    header,
    children,
    footer,
    label,
    ...props
}) => {
    return (
        <StyledContentWrapper {...props}>
            {header && (
                <>
                    <Box
                        sx={{
                            minWidth: '400px',
                            display: 'flex',
                            justifyContent: 'space-between',
                            padding: '14px 24px',
                            alignItems: 'center',
                        }}
                    >
                        <Typography sx={{ fontWeight: '500', fontSize: '18px' }}>
                            {label}
                        </Typography>
                        {header}
                    </Box>
                    <Divider />
                </>
            )}

            {children}
            {footer && (
                <>
                    <Divider />
                    <Box
                        sx={{
                            minWidth: '400px',
                            display: 'grid',
                            gridTemplateColumns: 'repeat(4, 1fr)',

                            padding: '14px 24px',
                            alignItems: 'center',
                        }}
                    >
                        {footer}
                    </Box>
                </>
            )}
        </StyledContentWrapper>
    );
};

interface StyledContentWrapperProps extends PaperProps {
    fullWidth?: boolean;
    fullHeight?: boolean;
}

const StyledContentWrapper = styled(Paper)<StyledContentWrapperProps>(
    {
        margin: '8px',
        borderRadius: '8px',
    },
    (props) => ({
        width: props.fullWidth ? '100%' : 'fit-content',
        height: props.fullHeight ? 'calc(100vh - 16px)' : 'fit-content',
    })
);
