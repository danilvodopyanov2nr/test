import { FC } from 'react';

import MuiButton, { ButtonProps } from '@mui/material/Button';

export const Button: FC<ButtonProps> = (props) => {
    const { children, ...otherProps } = props;

    return <MuiButton {...otherProps}>{children}</MuiButton>;
};
