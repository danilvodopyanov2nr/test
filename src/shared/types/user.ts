export type userRole = 'USER' | 'ADMIN';

export interface UserApiResponse {
    authorization: {
        description: {
            access_token: string;
            balance: number;
            type: number;
            language: number;
            url: string;
        };
    };
}

export interface User {
    access_token: string;
    session_id: string;
    balance: number;
    type: number;
    language_id: number;
    url: string;
}
