// import { AboutPage } from 'pages/AboutPage';
// import { LandingPage } from 'pages/LandingPage';
// import { Login } from 'pages/Login';
// import { Logout } from 'pages/Logout';
// import { MainPage } from 'pages/MainPage';
// import { NotFoundPage } from 'pages/NotFoundPage';
// import { RouteProps } from 'react-router-dom';

// export enum AppRoutes {
//     LANDING = 'landing',
//     MAIN = 'main',
//     ABOUT = 'about',
//     NOT_FOUND = 'not_found',
//     LOGIN = 'login',
//     LOGOUT = 'logout',
// }

// export const RoutePath: Record<AppRoutes, string> = {
//     [AppRoutes.LANDING]: '/',
//     [AppRoutes.NOT_FOUND]: '/*',
//     [AppRoutes.MAIN]: '/main',
//     [AppRoutes.ABOUT]: '/about',
//     [AppRoutes.LOGIN]: '/login',
//     [AppRoutes.LOGOUT]: '/logout',
// };

// export const routeConfig: Record<AppRoutes, RouteProps> = {
//     [AppRoutes.LANDING]: {
//         path: RoutePath.landing,
//         element: <LandingPage />,
//         children: [
//             {
//                 path: RoutePath.login,
//                 element: <Login />,
//             },
//         ],
//     },
//     [AppRoutes.MAIN]: {
//         path: RoutePath.main,
//         element: <MainPage />,
//     },
//     [AppRoutes.ABOUT]: {
//         path: RoutePath.about,
//         element: <AboutPage />,
//     },
//     [AppRoutes.NOT_FOUND]: {
//         path: RoutePath.not_found,
//         element: <NotFoundPage />,
//     },
//     [AppRoutes.LOGIN]: {
//         path: RoutePath.login,
//         element: <Login />,
//     },
//     [AppRoutes.LOGOUT]: {
//         path: RoutePath.logout,
//         element: <Logout />,
//     },
// };
