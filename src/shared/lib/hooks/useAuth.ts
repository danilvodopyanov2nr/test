import { useUserStore } from '../../../app/store/useUserStore/useUserStore';
import { User } from './../../types/user';
import { useState } from 'react';
import { loginUser } from 'shared/api/api';
import { LoginInput } from 'shared/types/inputTypes';
import { useMutation } from '@tanstack/react-query';

export const useAuth = () => {
    const localUser = JSON.parse(localStorage.getItem('user'));
    const isAuth = Boolean(localUser);
    const { user: userStore } = useUserStore();

    const [user, setUser] = useState<User>(localUser);

    const {
        mutate: loginUserQuery,
        isLoading: isLoadingLogin,
        isError,
        error,
        isSuccess,
    } = useMutation(
        (userData: LoginInput) =>
            loginUser({ ...userData, session_id: userStore.session_id }),
        {
            onSuccess: (user) => {
                setUser({
                    ...user.authorization.description,
                    language_id: userStore.language_id,
                    session_id: userStore.session_id,
                });
                localStorage.setItem(
                    'user',
                    JSON.stringify({
                        ...user.authorization.description,
                        session_id: userStore.session_id,
                    })
                );
            },
        }
    );
    const logout = () => {
        localStorage.removeItem('user');
    };
    return {
        loginUserQuery,
        user: { ...user, session_id: userStore.session_id },
        isAuth,
        isSuccess,
        isError,
        isLoadingLogin,
        error,
        logout,
    };
};
