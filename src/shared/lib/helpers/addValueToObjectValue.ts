interface AddValueToObjectValue<T> {
    object: T;
    value: string;
}

export const addValueToObjectValue = <T>({ object, value }: AddValueToObjectValue<T>) => {
    return Object.values(object).map((value) => value + '/');
};
