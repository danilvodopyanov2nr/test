import { ThemeProvider } from '@emotion/react';
import { createTheme, CssBaseline } from '@mui/material';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { ErrorBoundary } from 'app/providers/ErrorBoundary';

import WebSocketProvider from 'app/providers/WebSocket/WebSocketProvider';

import { ReactNode } from 'react';
import { BrowserRouter } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';

const queryClient = new QueryClient({
    defaultOptions: {
        queries: {
            refetchOnWindowFocus: false,
            refetchOnReconnect: false,
            retry: 1,
            staleTime: 5 * 1000,
        },
    },
});
const darkTheme = createTheme({
    palette: {
        secondary: {
            main: '#5C6BC0',
        },
        mode: 'light',
    },
});
export const AppProvider = ({ children }: { children: ReactNode }) => {
    return (
        <BrowserRouter>
            <ThemeProvider theme={darkTheme}>
                <CssBaseline />
                <ErrorBoundary>
                    <QueryClientProvider client={queryClient}>
                        {children}
                        <ToastContainer />
                    </QueryClientProvider>
                </ErrorBoundary>
            </ThemeProvider>
        </BrowserRouter>
    );
};
