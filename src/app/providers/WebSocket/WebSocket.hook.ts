import { WebSocketContext } from 'app/providers/WebSocket/WebSocketProvider';
import { useContext } from 'react';

export const useWebSocket = () => {
    const context = useContext(WebSocketContext);
    if (!context) {
        throw new Error('useWebsocket must be used within a WebSocketContext');
    }
    return context;
};
