import { Wallet, SystemWallet, ITariff } from 'app/store';

export interface GetWalletsTypesResponse {
    get_wallet_types: {
        task: string;
        wallet_types: Wallet[];
    };
}

export interface GetSystemWalletsResponse {
    get_system_wallets: {
        task: string;
        wallets: SystemWallet[];
    };
}

export interface GetTariffInfoResponse {
    get_tariff_info: {
        tariffs: ITariff[];
    };
}

export type webSocketAllResponse = GetWalletsTypesResponse &
    GetSystemWalletsResponse &
    GetTariffInfoResponse;

export type webSocketResponseKeys = keyof webSocketAllResponse;
