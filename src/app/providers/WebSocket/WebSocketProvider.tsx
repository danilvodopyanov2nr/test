import { useWebSocket } from 'app/providers/WebSocket/WebSocket.hook';
import { useWebSocketReducer } from 'app/providers/WebSocket/webSocketReducer';
import {
    webSocketAllResponse,
    webSocketResponseKeys,
} from 'app/providers/WebSocket/webSocketResponse.types';
import { Context, createContext, ReactNode, useEffect, useState } from 'react';
import { renewalAccessToken } from 'shared/api/api';

import { WSSURL } from 'shared/consts/apiUrl';
import { useAuth } from 'shared/lib/hooks/useAuth';
import { withRouter } from 'shared/lib/hooks/withRouter';

export interface IWebSocketContext {
    socket: WebSocket;
    status: any;
    sendMessage: (message: any) => void;
}

export const WebSocketContext: Context<Partial<IWebSocketContext>> = createContext({});

interface ISocketProvider {
    children: ReactNode;
}

export const useSendMessage = <T,>(message: any) => {
    const { socket, status } = useWebSocket();

    useEffect(() => {
        if (status.isOpen) {
            socket.send(JSON.stringify(message));
        }
    }, [status.isOpen]);
};

function WebSocketProvider({ children }: ISocketProvider) {
    const { isAuth, user } = useAuth();
    const [isSocketOpen, setIsSocketOpen] = useState(false);
    const [isSocketConnecting, setIsSocketConnecting] = useState(false);
    const [socket, setSocket] = useState<WebSocket | null>(null);
    const { setDataToStore } = useWebSocketReducer();
    let TIMEOUT = 250;

    const sendMessage = (message: any) => {
        socket.send(JSON.stringify(message));
    };

    useEffect(() => {
        let renewalWSToken: NodeJS.Timer;
        if (isAuth) {
            renewalWSToken = setInterval(() => {
                renewalAccessToken({
                    access_token: user.access_token,
                    session_id: user.session_id,
                });
            }, 1000 * 60 * 19);
        }

        return () => {
            clearInterval(renewalWSToken);
        };
    }, [isAuth]);

    useEffect(() => {
        connect();
    }, [user.access_token]);

    const check = () => {
        if (!socket || socket.readyState == WebSocket.CLOSED) connect(); //check if websocket instance is closed, if so call `connect` function.
    };

    useEffect(() => {
        if (isSocketOpen) {
            socket.onmessage = (message: MessageEvent<webSocketAllResponse>) => {
                const data = JSON.parse(message.data.toString()) as webSocketAllResponse;
                const key = Object.keys(data)[0] as webSocketResponseKeys;
                console.log('fff', key, Object.keys(data));
                setDataToStore(key, data);
                console.log('tut', data);
            };
        }
    }, [isSocketOpen]);

    const connect = () => {
        const ws = new WebSocket(WSSURL);

        let connectInterval: any;
        ws.onopen = () => {
            if (isAuth) {
                const accessToken = JSON.stringify({
                    access_token: user.access_token,
                });
                ws.send(accessToken);
            }
            console.log('Socket is start');
            setSocket(ws);
            TIMEOUT = 250;
            clearTimeout(connectInterval);
            setIsSocketOpen(true);
        };

        ws.onclose = (e) => {
            console.log(
                `Socket is closed. Reconnect will be attempted in ${Math.min(
                    10000 / 1000,
                    (TIMEOUT + TIMEOUT) / 1000
                )} second.`,
                e.reason
            );
            setIsSocketOpen(false);
            TIMEOUT = TIMEOUT + TIMEOUT; //increment retry interval
            connectInterval = setTimeout(check, Math.min(10000, TIMEOUT)); //call check function after timeout
        };

        ws.onerror = (err) => {
            console.error('Socket encountered error: ', err, 'Closing socket');
            setIsSocketOpen(false);
            ws.close();
        };
    };

    const status = {
        isOpen: isSocketOpen && !isSocketConnecting,
    };

    const value = { socket, status, sendMessage };
    return (
        <WebSocketContext.Provider value={value}>{children}</WebSocketContext.Provider>
    );
}
export default withRouter(WebSocketProvider);
