import {
    webSocketAllResponse,
    webSocketResponseKeys,
} from 'app/providers/WebSocket/webSocketResponse.types';
import { usePaymentStore, useTariffsStore } from 'app/store';

export const useWebSocketReducer = () => {
    const { payment, setPayment } = usePaymentStore();
    const { tariffs, setTariffs } = useTariffsStore();

    const setDataToStore = (key: webSocketResponseKeys, data: webSocketAllResponse) => {
        const value = data[key];
        console.log(payment, value, { ...payment, ...value });
        switch (key) {
            case 'get_wallet_types':
                setPayment({
                    ...payment,
                    ...value,
                });
            case 'get_system_wallets':
                setPayment({
                    ...payment,
                    ...value,
                });
            case 'get_tariff_info': {
                if ('tariffs' in value) setTariffs([...tariffs, ...value.tariffs]);
            }
            default:
                break;
        }
    };

    return { setDataToStore };
};
