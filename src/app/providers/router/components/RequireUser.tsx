import { RoutePath } from 'app/providers/router/config/routerConfig';
import { Navigate, Outlet, useLocation } from 'react-router-dom';
import { useAuth } from 'shared/lib/hooks/useAuth';

const RequireUser = ({ allowedRoles }: { allowedRoles: string[] }) => {
    const location = useLocation();

    const { isLoadingLogin, isAuth } = useAuth();

    if (isLoadingLogin) {
        return <div>Loading...</div>;
    }

    return isAuth ? (
        <Outlet />
    ) : (
        <Navigate to={RoutePath.login} state={{ from: location }} replace />
    );
};

export default RequireUser;
