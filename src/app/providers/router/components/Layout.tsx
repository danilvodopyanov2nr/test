import { Box } from '@mui/material';
import { RoutePath } from 'app/providers/router/config/routerConfig';
import { useState } from 'react';
import { Outlet, useLocation } from 'react-router-dom';
import { DrawerMenu, MenuItem } from 'shared';

import { Navbar } from 'widgets/Navbar';
import { navbarPageMapper } from 'widgets/Navbar/utils/converter';

const pages: (keyof typeof navbarPageMapper)[] = ['Trading', 'Market', 'Signals'];

export const Layout = () => {
    const [isOpen, setIsOpen] = useState(false);
    const location = useLocation();
    const MENU_ITEMS: MenuItem[] = [
        { name: 'Реферальная система', path: '' },
        { name: 'Оплата', path: RoutePath.tariff },
        { name: 'Обратная связь', path: '' },
    ];
    return (
        <>
            <Box display="flex" flexDirection="row">
                <Box flexGrow={0}>
                    <Navbar
                        isOpen={isOpen}
                        setIsOpen={setIsOpen}
                        activePage={location.pathname}
                        pages={pages}
                    />
                </Box>
                <Box display="flex" flexDirection="row" flexGrow={1}>
                    <Box display="flex" flexDirection="column" flexGrow={1}>
                        <DrawerMenu
                            setIsOpen={setIsOpen}
                            isOpen={isOpen}
                            appBarWidth={64}
                            drawerWidth={220}
                            menuItems={MENU_ITEMS}
                        ></DrawerMenu>
                        <Outlet />
                    </Box>
                </Box>
            </Box>
        </>
    );
};
