export { Layout } from 'app/providers/router/components/Layout';
import RequireUser from 'app/providers/router/components/RequireUser';
import routes from 'app/providers/router/config/routerConfig';

export { routes, RequireUser };
