import { Layout } from 'app/providers/router/components/Layout';
import RequireUser from 'app/providers/router/components/RequireUser';

import { ForgotPasswordPage } from 'pages/ForgotPasswordPage';
import { LandingPage } from 'pages/LandingPage';
import { Login } from 'pages/Login';
import { Logout } from 'pages/Logout';
import { MainPage } from 'pages/MainPage';
import { MarketPage } from 'pages/MarketPage';
import { Registration } from 'pages/Registration';
import { SignalsPage } from 'pages/SignalsPage';
import { TariffPage } from 'pages/TariffPage';
import { Verification } from 'pages/VerificationPage';

import { ComponentType, Suspense } from 'react';
import { RouteObject } from 'react-router-dom';

export enum AppRoutes {
    LANDING = 'landing',
    MAIN = 'main',
    NOT_FOUND = 'not_found',
    LOGIN = 'login',
    LOGOUT = 'logout',
    REGISTRATION = 'registration',
    VERIFICATION = 'verification',
    MARKET = 'market',
    TARIFF = 'tariff',
    SIGNALS = 'signals',
    FORGOT_PASSWORD = 'forgot-password',
}

export const RoutePath: Record<AppRoutes, string> = {
    [AppRoutes.LANDING]: '/',
    [AppRoutes.NOT_FOUND]: '/*',
    [AppRoutes.MAIN]: '/main',
    [AppRoutes.LOGIN]: '/login',
    [AppRoutes.LOGOUT]: '/logout',
    [AppRoutes.REGISTRATION]: '/registration',
    [AppRoutes.VERIFICATION]: '/verification',
    [AppRoutes.MARKET]: '/market',
    [AppRoutes.TARIFF]: '/tariff',
    [AppRoutes.SIGNALS]: '/signals',
    [AppRoutes.FORGOT_PASSWORD]: '/forgot-password',
};

const Loadable = (Component: ComponentType<any>) => (props: JSX.IntrinsicAttributes) =>
    (
        <Suspense fallback={<p>Loading...</p>}>
            <Component {...props} />
        </Suspense>
    );

const LoginPage = Loadable(Login);
const Landing = Loadable(LandingPage);
const Main = Loadable(MainPage);
const LogoutPage = Loadable(Logout);
const RegistrationPage = Loadable(Registration);
const VerificationPage = Loadable(Verification);
const Market = Loadable(MarketPage);
const Tariff = Loadable(TariffPage);
const Signals = Loadable(SignalsPage);
const ForgotPassword = Loadable(ForgotPasswordPage);

const authRoutes: RouteObject = {
    path: '*',
    children: [
        {
            path: AppRoutes.LOGIN,
            element: <LoginPage />,
        },
        {
            path: AppRoutes.LOGOUT,
            element: <LogoutPage />,
        },
        {
            path: AppRoutes.REGISTRATION,
            element: <RegistrationPage />,
        },
        {
            path: AppRoutes.VERIFICATION,
            element: <VerificationPage />,
        },
        {
            path: AppRoutes.VERIFICATION,
            element: <VerificationPage />,
        },
        {
            path: AppRoutes.FORGOT_PASSWORD,
            element: <ForgotPassword />,
        },
    ],
};

const normalRoutes: RouteObject = {
    path: '*',
    element: <Layout />,
    children: [
        {
            index: true,
            element: <Landing />,
        },
        {
            path: AppRoutes.MAIN,
            element: <RequireUser allowedRoles={['user', 'admin']} />,
            children: [
                {
                    path: '',
                    element: <Main />,
                },
            ],
        },
        {
            path: AppRoutes.MARKET,
            element: <Market />,
        },
        {
            path: AppRoutes.TARIFF,
            element: <Tariff />,
        },
        {
            path: AppRoutes.SIGNALS,
            element: <Signals />,
        },
    ],
};

const routes: RouteObject[] = [authRoutes, normalRoutes];

export default routes;
