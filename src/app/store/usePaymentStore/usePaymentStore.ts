import { Payment } from './usePaymentStore.types';
import { create } from 'zustand';

interface PaymentState {
    payment: Payment;
    setPayment: (payment: Payment) => void;
}

export const usePaymentStore = create<PaymentState>((set) => ({
    payment: {},
    setPayment: (newPayment) => {
        set((state) => ({ ...state, payment: { ...state.payment, ...newPayment } }));
    },
}));
