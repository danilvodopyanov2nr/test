export interface SystemWallet {
    num: string;
    name: string;
    symbol: string;
}

export interface Wallet {
    id: number;
    name: string;
    symbol: string;
}

export interface Payment {
    wallets?: SystemWallet[];
    wallet_types?: Wallet[];
}
