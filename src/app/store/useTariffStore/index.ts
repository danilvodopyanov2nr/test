export { useTariffsStore } from './useTariffsStore';

export type { ITariff, ITariffsPeriod } from './useTariffsStore.types';
