import { ITariff } from 'app/store/useTariffStore/useTariffsStore.types';
import { create } from 'zustand';

interface TariffState {
    tariffs: ITariff[];
    setTariffs: (tariffs: ITariff[]) => void;
}

export const useTariffsStore = create<TariffState>((set) => ({
    tariffs: [],
    setTariffs: (tariffs) => set((state) => ({ ...state, tariffs })),
}));
