export interface ITariffsPeriod {
    period_id: number;
    name: string;
    period: number;
    price: number;
}

export interface ITariff {
    tariffs_id: number;
    name: string;
    descr: string;
    percent_from_deal: number;
    min_fee_from_deal: number;
    max_fee_from_deal: number;
    for_trader: boolean;
    tariffs_period: ITariffsPeriod[];
}
