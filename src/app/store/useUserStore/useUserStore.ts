import { PostRegistrationUser } from 'shared/api/types';
import { generateSessionId } from 'shared/lib/helpers/generateSessionId';
import { User } from 'shared/types/user';
import { create } from 'zustand';

interface UserState {
    user: Partial<User>;
    addUser: (user: User) => void;
    registrationUserData: Partial<PostRegistrationUser>;
    addRegistrationUserData: (user: PostRegistrationUser) => void;
    requestLoading: boolean;
    setRequestLoading: (isLoading: boolean) => void;
}

export const useUserStore = create<UserState>((set) => ({
    user: {
        ...JSON.parse(localStorage.getItem('user')),
        session_id: generateSessionId(),
    },
    addUser: (user) => set((state) => ({ user: user })),
    requestLoading: false,
    setRequestLoading: (isLoading) =>
        set((state) => ({ ...state, requestLoading: isLoading })),
    registrationUserData: {},
    addRegistrationUserData: (user) => set((state) => ({ registrationUserData: user })),
}));
