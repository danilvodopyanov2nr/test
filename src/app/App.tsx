import './styles/index.css';

import { useRoutes } from 'react-router-dom';
import routes from 'app/providers/router/config/routerConfig';
import 'react-toastify/dist/ReactToastify.min.css';
import styled from '@emotion/styled';

export const App = () => {
    const content = useRoutes(routes);

    return <AppContainer>{content}</AppContainer>;
};

const AppContainer = styled.div`
    width: 100vw;
    height: 100vh;
    background: #f5f5f5;
`;
