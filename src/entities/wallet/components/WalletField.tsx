import { Select, MenuItem, Typography } from '@mui/material';
import { WalletFieldProps } from 'entities/wallet/wallet.types';
import { FC } from 'react';

export const WalletField: FC<WalletFieldProps> = ({ field, wallets, ...props }) => {
    return (
        <Select fullWidth id="type-select" {...props} {...field}>
            {wallets?.length ? (
                wallets.map((t) => (
                    <MenuItem key={'wallet' + t.num} value={t.num}>
                        {t.num}
                    </MenuItem>
                ))
            ) : (
                <Typography>Кошельки не найдены</Typography>
            )}
        </Select>
    );
};
