import { SelectProps } from '@mui/material';
import { SystemWallet } from 'app/store';

export interface WalletFieldProps extends SelectProps {
    field: any;
    wallets: SystemWallet[];
}
