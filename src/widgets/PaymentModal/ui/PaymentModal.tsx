import { zodResolver } from '@hookform/resolvers/zod';
import { Box, MenuItem, Select, Typography, styled } from '@mui/material';

import { WalletField } from 'entities/wallet';

import { useEffect } from 'react';
import { FormProvider, Controller, useForm } from 'react-hook-form';
import { object, z, TypeOf } from 'zod';
import CheckRoundedIcon from '@mui/icons-material/CheckRounded';
import CloseRoundedIcon from '@mui/icons-material/CloseRounded';
import { usePaymentStore } from 'app/store';
import { useWalletProvider } from 'features';

const paymentSchema = object({
    symbol: z.string(),
    net: z.string(),
    wallet: z.string(),
});

export type PaymentProps = TypeOf<typeof paymentSchema>;

export const PaymentForm = () => {
    const { setWalletId } = useWalletProvider();
    const { payment } = usePaymentStore();
    const methods = useForm<PaymentProps>({
        resolver: zodResolver(paymentSchema),
        defaultValues: {
            symbol: '',
            net: '',
            wallet: '',
        },
    });
    const {
        reset,
        control,
        handleSubmit,
        getValues,
        watch,
        formState: { defaultValues, isSubmitSuccessful, errors },
    } = methods;
    const formState = watch();
    useEffect(() => {
        console.log('pay', payment);
    }, [payment]);

    useEffect(() => {
        setWalletId(Number(formState.net));
    }, [formState.net]);

    const onSubmit = async () => {};

    return (
        <FormProvider {...methods}>
            <form style={{ width: '100%' }} onSubmit={handleSubmit(onSubmit)}>
                <Box sx={{ padding: '24px 16px 24px 24px' }}>
                    <Box sx={{ display: 'flex', gap: '16px' }}>
                        <Controller
                            name="symbol"
                            control={control}
                            render={({ field }) => (
                                <Select
                                    fullWidth
                                    defaultValue={defaultValues.symbol}
                                    displayEmpty
                                    renderValue={(selected) => {
                                        return selected.length === 0 ? (
                                            <Typography>Символ</Typography>
                                        ) : (
                                            selected
                                        );
                                    }}
                                    sx={{
                                        height: '40px',
                                    }}
                                    {...field}
                                >
                                    {payment?.wallet_types &&
                                        payment?.wallet_types.map((wallet, index) => (
                                            <MenuItem
                                                key={'symbol' + wallet.symbol + index}
                                                value={wallet.symbol}
                                            >
                                                {wallet.symbol}
                                            </MenuItem>
                                        ))}
                                </Select>
                            )}
                        />
                        {errors.wallet && (
                            <ErrorMessage>{errors.wallet.message}</ErrorMessage>
                        )}
                        <Controller
                            name="net"
                            control={control}
                            render={({ field }) => (
                                <Select
                                    fullWidth
                                    defaultValue={defaultValues.net}
                                    disabled={!Boolean(formState.symbol)}
                                    displayEmpty
                                    renderValue={(selected) => {
                                        return Number(selected) == 0 ? (
                                            <Typography>Сеть</Typography>
                                        ) : (
                                            payment?.wallet_types?.find((wallet) => {
                                                return wallet.id === Number(selected);
                                            }).name
                                        );
                                    }}
                                    sx={{ height: '40px' }}
                                    {...field}
                                >
                                    {payment?.wallet_types &&
                                        payment?.wallet_types
                                            .filter(
                                                (wallet) =>
                                                    wallet.symbol === formState.symbol
                                            )
                                            .map((wallet, index) => (
                                                <MenuItem
                                                    key={'net' + wallet.name + index}
                                                    value={wallet.id}
                                                >
                                                    {wallet.name}
                                                </MenuItem>
                                            ))}
                                </Select>
                            )}
                        />
                        {errors.wallet && (
                            <ErrorMessage>{errors.wallet.message}</ErrorMessage>
                        )}
                        <Controller
                            name="wallet"
                            control={control}
                            render={({ field }) => (
                                <WalletField
                                    defaultValue={defaultValues.wallet}
                                    displayEmpty
                                    renderValue={(selected: string) => {
                                        return selected.length === 0 ? (
                                            <Typography>Кошелек</Typography>
                                        ) : (
                                            selected
                                        );
                                    }}
                                    sx={{
                                        height: '40px',
                                    }}
                                    disabled={!Boolean(formState.net)}
                                    field={field}
                                    wallets={payment.wallets}
                                />
                            )}
                        />
                        {errors.wallet && (
                            <ErrorMessage>{errors.wallet.message}</ErrorMessage>
                        )}
                        <Box sx={{ display: 'flex', alignItems: 'center', gap: '20px' }}>
                            <CheckRoundedIcon color="success" />
                            <CloseRoundedIcon color="error" onClick={() => reset()} />
                        </Box>
                    </Box>
                </Box>
            </form>
        </FormProvider>
    );
};

const ErrorMessage = styled(Typography)({
    color: 'red',
    fontSize: '12px',
});
