export interface NavbarPage {
    name: string;
    link: string;
    icon: any;
}
