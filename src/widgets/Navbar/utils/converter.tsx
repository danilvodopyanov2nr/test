import { NavbarPage } from 'widgets/Navbar/types/Navbar.types';
import SensorsIcon from '@mui/icons-material/Sensors';
import AccountBalanceIcon from '@mui/icons-material/AccountBalance';
import BarChartIcon from '@mui/icons-material/BarChart';

export type NavbarPageName = 'Trading' | 'Market' | 'Signals';

export const navbarPageMapper: Record<NavbarPageName, NavbarPage> = {
    Trading: {
        name: 'Трейдинг',
        icon: BarChartIcon,
        link: '/main',
    },
    Market: {
        name: 'Биржи',
        icon: AccountBalanceIcon,
        link: '/market',
    },
    Signals: {
        name: 'Сигналы',
        icon: SensorsIcon,
        link: '/signals',
    },
};

export const convertNavbarApiPage = (pageName: NavbarPageName) => {
    return navbarPageMapper[pageName];
};
