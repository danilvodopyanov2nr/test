import { AppBar, Box, colors, IconButton, Toolbar, Typography } from '@mui/material';

import styled from '@emotion/styled';

import { RoutePath } from 'app/providers/router/config/routerConfig';

import React, { FC, PropsWithChildren, ReactElement } from 'react';

import { useAuth } from 'shared/lib/hooks/useAuth';

import { AppLink } from 'shared/ui/AppLink/AppLink';

import MenuIcon from '@mui/icons-material/Menu';
import { convertNavbarApiPage, NavbarPageName } from 'widgets/Navbar/utils/converter';

interface INavbarProps {
    setIsOpen?: any;
    isOpen: boolean;
    pages?: string[];
    activePage?: string;
}

const settings = ['Profile', 'Account', 'Dashboard', 'Logout'];

const Navbar: FC<INavbarProps> = ({ setIsOpen, isOpen, pages, activePage }) => {
    const { isAuth } = useAuth();

    const convertedPage = pages
        .map((page: NavbarPageName) => convertNavbarApiPage(page))
        .map((page) => {
            const IconComponent = page.icon;
            const StyledIcon = styled(IconComponent)``;
            return {
                ...page,
                icon: <StyledIcon />,
            };
        });
    console.log('dasd', activePage, convertedPage);
    const [anchorElNav, setAnchorElNav] = React.useState<null | HTMLElement>(null);
    const [anchorElUser, setAnchorElUser] = React.useState<null | HTMLElement>(null);

    const handleOpenNavMenu = (event: React.MouseEvent<HTMLElement>) => {
        setIsOpen((prev: boolean) => !prev);
        setAnchorElNav(event.currentTarget);
    };

    const StyledNavbarText: FC<
        PropsWithChildren<{ icon?: ReactElement; isActive?: boolean }>
    > = ({ children, icon, isActive = false }) => (
        <Box
            sx={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                padding: '10px',
                path: {
                    fill: 'rgba(0, 0, 0, 0.6)',
                },
                ...(isActive && {
                    background: '#2196F3',
                    color: '#fff',
                    path: {
                        fill: '#fff',
                    },
                    span: {
                        color: '#fff',
                    },
                }),
                ':hover': {
                    background: '#2196F3',
                    color: '#fff',
                    path: {
                        fill: '#fff',
                    },
                    span: {
                        color: '#fff',
                    },
                },
            }}
        >
            {icon}
            <Typography
                component="span"
                variant="subtitle1"
                fontSize={12}
                color="rgba(0, 0, 0, 0.6)"
                textAlign="center"
                sx={{ marginTop: '8px', wordBreak: 'break-all' }}
            >
                {children}
            </Typography>
        </Box>
    );
    return (
        <AppBar
            elevation={0}
            position="sticky"
            sx={{
                height: '100vh',
                maxWidth: '62px',
                background: '#fff',
                zIndex: (theme) => theme.zIndex.drawer + 1,
            }}
        >
            <Toolbar disableGutters sx={{ flexDirection: 'column' }}>
                <Box
                    sx={{
                        flexGrow: 1,
                        display: 'flex',
                        flexDirection: 'column',
                    }}
                >
                    <BurgerMenu
                        size="large"
                        aria-label="account of current user"
                        aria-controls="menu-appbar"
                        onClick={handleOpenNavMenu}
                        color="default"
                    >
                        <MenuIcon />
                    </BurgerMenu>
                    {isAuth &&
                        convertedPage.map((page) => (
                            <AppLink key={page.name} to={page.link}>
                                <StyledNavbarText
                                    isActive={activePage === page.link}
                                    icon={page.icon}
                                >
                                    {page.name}
                                </StyledNavbarText>
                            </AppLink>
                        ))}
                    <AppLink to={!isAuth ? RoutePath.login : RoutePath.logout}>
                        <StyledNavbarText>
                            {!isAuth ? 'Log in' : 'Log out'}
                        </StyledNavbarText>
                    </AppLink>
                    {!isAuth && (
                        <AppLink to={RoutePath.registration}>
                            <StyledNavbarText>Sign in</StyledNavbarText>
                        </AppLink>
                    )}
                </Box>
            </Toolbar>
        </AppBar>
    );
};

const BurgerMenu = styled(IconButton)({
    '&:hover': {
        backgroundColor: 'transparent',
        '& svg': {
            fill: colors.blue[500],
        },
    },
});
export { Navbar };
