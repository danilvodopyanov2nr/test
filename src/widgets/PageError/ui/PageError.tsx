import { Button } from 'shared/ui/Button';

export const PageError: React.FC = (props) => {
    const reloadPage = () => {
        location.reload();
    };

    return (
        <div>
            <p>Что-то случилось</p>
            <Button onClick={reloadPage}>Обновить страницу</Button>
        </div>
    );
};
