import { Box, Button, MenuItem, MenuList, Modal, Typography } from '@mui/material';

import { useWebSocket } from 'app/providers/WebSocket/WebSocket.hook';
import { useUserStore } from 'app/store/useUserStore/useUserStore';
import { MarketPageForm } from 'pages/MarketPage/ui/MarketPage.form';
import { useEffect, useState } from 'react';
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';

import { getMarkets } from 'shared/api/api';
import { Market } from 'shared/api/types';

import { useNavigate } from 'react-router-dom';
import { RoutePath } from 'app/providers/router/config/routerConfig';
import { ContentWrapper } from 'shared';

interface DeleteMarketResponse {
    delete_credentials_api: {
        done: boolean;
    };
}

const MarketPage = () => {
    const { sendMessage, status, socket } = useWebSocket();
    const navigate = useNavigate();
    const [newMarket, setNewMarket] = useState(null);
    const [currentMarket, setCurrentMarket] = useState(null);
    const { user } = useUserStore();
    const [markets, setMarkets] = useState<Market[]>();
    const [open, setOpen] = useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);

    const removeMarket = (apiId: number) => {
        if (status.isOpen) {
            sendMessage({
                delete_credentials_api: { api_id: apiId },
            });
        }
        socket.onmessage = (event: MessageEvent<DeleteMarketResponse>) => {
            const data = JSON.parse(event.data.toString()) as DeleteMarketResponse;

            if (data.delete_credentials_api.done) {
                const newData = markets.filter((market) => market.api_id !== apiId);
                setMarkets(newData);
                setNewMarket(null);
                setCurrentMarket(null);
            }
        };
    };

    useEffect(() => {
        if (user.type === 2) navigate(RoutePath.tariff);
        getMarkets(user.access_token).then((res) => setMarkets(res.get_user_api_id));
    }, []);

    const handleSetMarket = (api_id: number) => {
        const newCurrentMarket = markets.find((market) => market.api_id === api_id);
        setCurrentMarket(newCurrentMarket);
        setNewMarket(null);
    };

    const handleSaveMarket = (value: any) => {
        setNewMarket(null);
        setCurrentMarket(value);
        setMarkets([...markets, value]);
    };

    const handleAddMarket = () => {
        setCurrentMarket(null);
        setNewMarket(true);
    };

    console.log(newMarket);
    return (
        <Box sx={{ display: 'flex' }}>
            <ContentWrapper
                label="Биржи"
                fullHeight={true}
                header={
                    <Button
                        variant="contained"
                        startIcon={<AddCircleOutlineIcon />}
                        onClick={handleAddMarket}
                        color="secondary"
                    >
                        Добавить
                    </Button>
                }
            >
                <Modal
                    open={open}
                    onClose={handleClose}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description"
                >
                    <Box sx={style}>
                        <Typography id="modal-modal-title" variant="h6" component="h2">
                            Подключение новой биржи
                        </Typography>
                    </Box>
                </Modal>
                <MenuList>
                    {markets
                        ?.filter((value) => Boolean(value.name))
                        ?.map((market) => (
                            <MenuItem
                                sx={{
                                    padding: '16px',
                                    fontWeight: '400',
                                    color:
                                        currentMarket?.api_id === market.api_id
                                            ? '#5C6BC0'
                                            : 'inherit',
                                    borderRight:
                                        currentMarket?.api_id === market.api_id
                                            ? '2px solid #5C6BC0;'
                                            : 'none',
                                    background:
                                        currentMarket?.api_id === market.api_id
                                            ? '#5C6BC00A'
                                            : 'none',
                                    '&:hover': {
                                        color: '#5C6BC0',
                                        background: '#5C6BC00A',
                                    },
                                }}
                                key={market.api_id}
                                onClick={() => handleSetMarket(market.api_id)}
                            >
                                {market.name}
                            </MenuItem>
                        ))}
                </MenuList>
            </ContentWrapper>
            {newMarket && (
                <ContentWrapper>
                    <MarketPageForm
                        removeMarket={removeMarket}
                        handleSumbit={handleSaveMarket}
                    />
                </ContentWrapper>
            )}
            {currentMarket && (
                <ContentWrapper>
                    <MarketPageForm
                        removeMarket={removeMarket}
                        handleSumbit={handleSaveMarket}
                        defaultValue={currentMarket}
                    />
                </ContentWrapper>
            )}
        </Box>
    );
};

const style = {
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    boxShadow: 24,
    p: 4,
};

export default MarketPage;
