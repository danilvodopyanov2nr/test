import { zodResolver } from '@hookform/resolvers/zod';

import {
    Box,
    Button,
    Divider,
    MenuItem,
    Select,
    styled,
    TextField,
    TextFieldProps,
    Typography,
} from '@mui/material';
import { useWebSocket } from 'app/providers/WebSocket/WebSocket.hook';
import SaveIcon from '@mui/icons-material/Save';

import { FC, useEffect, useMemo } from 'react';
import { FormProvider, Controller, useForm } from 'react-hook-form';
import * as z from 'zod';
import { Market } from 'shared/api/types';

const addMarketSchema = z.object({
    name: z.string().min(1, 'Введите название'),
    type: z.number(),
    key: z.string().min(1, 'Введите api key'),
    secret: z.string().min(1, 'Введите api secret'),
});

export type AddMarketForm = Required<z.infer<typeof addMarketSchema>>;

interface MarketPageFormProps {
    defaultValue?: AddMarketForm & { api_id: number };
    handleSumbit?: (values: Market) => void;
    removeMarket: any;
}

export const MarketPageForm: FC<MarketPageFormProps> = ({
    defaultValue,
    handleSumbit,
    removeMarket,
}) => {
    const { sendMessage, status, socket } = useWebSocket();

    const methods = useForm({
        resolver: zodResolver(addMarketSchema),
        defaultValues: useMemo(() => {
            console.log('d', defaultValue);
            return (
                defaultValue || {
                    name: '',
                    type: 0,
                    key: '',
                    secret: '',
                }
            );
        }, [defaultValue]),
    });
    const {
        reset,
        control,
        handleSubmit,
        getValues,
        formState: { defaultValues, isSubmitSuccessful, errors },
    } = methods;

    useEffect(() => {
        reset();
    }, [defaultValues]);

    useEffect(() => {
        reset(defaultValue);
    }, [defaultValue]);

    const onSubmit = async () => {
        if (status.isOpen) {
            sendMessage({ create_credentials_api: { ...getValues() } });
        }

        socket.onmessage = (event: any) => {
            const data = JSON.parse(event.data.toString()) as any;

            if (data.create_credentials_api.api_id) {
                handleSumbit({
                    ...getValues(),
                    api_id: data.create_credentials_api.api_id,
                });
            }
        };
    };

    return (
        <FormProvider {...methods}>
            <form style={{ width: '100%' }} onSubmit={handleSubmit(onSubmit)}>
                <Box sx={{ padding: '8px 24px' }}>
                    <Controller
                        name="name"
                        control={control}
                        render={({ field }) => (
                            <LoginInput
                                variant="outlined"
                                size="small"
                                placeholder="Название"
                                fullWidth
                                {...field}
                            />
                        )}
                    />
                    {errors.name && <ErrorMessage>{errors.name.message}</ErrorMessage>}
                    <Controller
                        name="type"
                        control={control}
                        render={({ field }) => (
                            <Select
                                fullWidth
                                id="type-select"
                                sx={{ marginTop: '16px', height: '40px' }}
                                {...field}
                            >
                                <MenuItem key={'Binance'} value={0}>
                                    Binance
                                </MenuItem>
                                <MenuItem key={'Forex'} value={1}>
                                    Forex
                                </MenuItem>
                            </Select>
                        )}
                    />
                    {errors.secret && (
                        <ErrorMessage>{errors.secret.message}</ErrorMessage>
                    )}
                    <Controller
                        name="secret"
                        control={control}
                        render={({ field }) => (
                            <LoginInput
                                variant="outlined"
                                size="small"
                                placeholder="API Secret"
                                fullWidth
                                {...field}
                            />
                        )}
                    />
                    {errors.secret && (
                        <ErrorMessage>{errors.secret.message}</ErrorMessage>
                    )}
                    <Controller
                        name="key"
                        control={control}
                        render={({ field }) => (
                            <LoginInput
                                variant="outlined"
                                size="small"
                                placeholder="API Key"
                                fullWidth
                                {...field}
                            />
                        )}
                    />
                    {errors.key && <ErrorMessage>{errors.key.message}</ErrorMessage>}
                </Box>
                <Divider />
                <Box
                    sx={{
                        minWidth: '400px',
                        display: 'grid',
                        gridTemplateColumns: 'repeat(4, 1fr)',

                        padding: '14px 24px',
                        alignItems: 'center',
                    }}
                >
                    {defaultValue?.api_id && (
                        <Button
                            onClick={() => removeMarket(defaultValue.api_id)}
                            variant="contained"
                            startIcon={<SaveIcon />}
                            color="error"
                            sx={{ gridColumnStart: 1 }}
                        >
                            Удалить
                        </Button>
                    )}
                    <Button
                        type="submit"
                        variant="contained"
                        startIcon={<SaveIcon />}
                        color="secondary"
                        sx={{ gridColumnStart: 4 }}
                    >
                        Сохранить
                    </Button>
                </Box>
            </form>
        </FormProvider>
    );
};

const LoginInput = styled(TextField)<TextFieldProps>(() => ({
    marginTop: '16px',
    '&:last-child': {
        marginBottom: '16px',
    },
}));

const LoginButton = styled(Button)({
    marginTop: '50px',
    backgroundColor: '#ffffff',
    color: '#080710',
    padding: ' 15px 0',
    fontWeight: '600',
    '&:hover': {
        backgroundColor: '#ffffffea',
    },
});

const LabelInput = styled(Typography)({
    marginTop: '30px',
    fontSize: '16px',
    fontWeight: '500',
});

const ErrorMessage = styled(Typography)({
    color: 'red',
    fontSize: '12px',
});
