interface NotFoundPageProps {
    className?: string;
}

export const NotFoundPage: React.FC<NotFoundPageProps> = (props) => {
    return <div>Страница не найдена</div>;
};
