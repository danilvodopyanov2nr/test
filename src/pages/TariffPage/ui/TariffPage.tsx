import { TabContext, TabList, TabListProps, TabPanel } from '@mui/lab';
import {
    Box,
    Button,
    MenuItem,
    MenuList,
    styled,
    Tab,
    TabProps,
    Typography,
} from '@mui/material';
import { useWebSocket } from 'app/providers/WebSocket/WebSocket.hook';

import { useUserStore } from 'app/store/useUserStore/useUserStore';
import React, { useEffect, useState } from 'react';

import CheckRoundedIcon from '@mui/icons-material/CheckRounded';
import { useTariffsStore } from 'app/store/useTariffStore/useTariffsStore';

import { ITariff } from 'app/store';
import { ContentWrapper } from 'shared';

const TariffPage = () => {
    const { sendMessage, status, socket } = useWebSocket();

    const [value, setValue] = React.useState('1');
    const { tariffs } = useTariffsStore();
    const [currentTariff, setCurrentTariff] = useState<ITariff>(null);
    const handleChange = (event: React.SyntheticEvent, newValue: string) => {
        setValue(newValue);
    };

    useEffect(() => {
        if (status.isOpen) {
            sendMessage({ get_tariff_info: {} });
        }
    }, [status.isOpen]);

    const handleSetTariff = (tariffs_id: number) => {
        const newCurrentTariff = tariffs.find(
            (tariff) => tariff.tariffs_id === tariffs_id
        );
        setCurrentTariff(newCurrentTariff);
    };
    const { user } = useUserStore();
    return (
        <Box sx={{ display: 'flex' }}>
            <ContentWrapper
                label="Оплата"
                fullHeight
                header={
                    <Box sx={{ display: 'flex', alignItems: 'baseline' }}>
                        <Typography
                            sx={{
                                fontSize: '14px',
                                color: '#00000099',
                                fontWeight: '500',
                            }}
                        >
                            Баланс&nbsp;
                        </Typography>
                        <Typography sx={{ fontWeight: '700' }}>
                            ${user.balance}
                        </Typography>
                    </Box>
                }
            >
                <Box sx={{ width: '100%', typography: 'body1' }}>
                    <TabContext value={value}>
                        <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                            <StyledTabs
                                textColor="secondary"
                                indicatorColor="secondary"
                                onChange={handleChange}
                                aria-label="lab API tabs example"
                            >
                                <StyledTab label="Для трейдера" value="1" />
                                <StyledTab label="Для поставщика сигналов" value="2" />
                            </StyledTabs>
                        </Box>
                        <TabPanel sx={{ padding: '0' }} value="1">
                            <MenuList>
                                {tariffs
                                    ?.filter((tarrif) => tarrif.for_trader)
                                    .map((tarrif) => {
                                        return (
                                            <MenuItem
                                                sx={{
                                                    padding: '16px',
                                                    fontWeight: '400',
                                                    color:
                                                        currentTariff?.tariffs_id ===
                                                        tarrif.tariffs_id
                                                            ? '#5C6BC0'
                                                            : 'inherit',
                                                    borderRight:
                                                        currentTariff?.tariffs_id ===
                                                        tarrif.tariffs_id
                                                            ? '2px solid #5C6BC0;'
                                                            : 'none',
                                                    background:
                                                        currentTariff?.tariffs_id ===
                                                        tarrif.tariffs_id
                                                            ? '#5C6BC00A'
                                                            : 'none',
                                                    '&:hover': {
                                                        color: '#5C6BC0',
                                                        background: '#5C6BC00A',
                                                    },
                                                }}
                                                key={tarrif.tariffs_id}
                                                onClick={() =>
                                                    handleSetTariff(tarrif.tariffs_id)
                                                }
                                            >
                                                {tarrif.name}
                                            </MenuItem>
                                        );
                                    })}
                            </MenuList>
                        </TabPanel>
                        <TabPanel sx={{ padding: '0' }} value="2">
                            <MenuList>
                                {tariffs
                                    ?.filter((tarrif) => !tarrif.for_trader)
                                    .map((tarrif) => {
                                        return (
                                            <MenuItem
                                                sx={{
                                                    padding: '16px',
                                                    fontWeight: '400',
                                                    color:
                                                        currentTariff?.tariffs_id ===
                                                        tarrif.tariffs_id
                                                            ? '#5C6BC0'
                                                            : 'inherit',
                                                    borderRight:
                                                        currentTariff?.tariffs_id ===
                                                        tarrif.tariffs_id
                                                            ? '2px solid #5C6BC0;'
                                                            : 'none',
                                                    background:
                                                        currentTariff?.tariffs_id ===
                                                        tarrif.tariffs_id
                                                            ? '#5C6BC00A'
                                                            : 'none',
                                                    '&:hover': {
                                                        color: '#5C6BC0',
                                                        background: '#5C6BC00A',
                                                    },
                                                }}
                                                key={tarrif.tariffs_id}
                                                onClick={() =>
                                                    handleSetTariff(tarrif.tariffs_id)
                                                }
                                            >
                                                {tarrif.name}
                                            </MenuItem>
                                        );
                                    })}
                            </MenuList>
                        </TabPanel>
                    </TabContext>
                </Box>
            </ContentWrapper>
            {currentTariff && (
                <ContentWrapper
                    label={currentTariff.name}
                    header={
                        <Box sx={{ display: 'flex', alignItems: 'baseline' }}>
                            <Typography sx={{ fontWeight: '700' }}>
                                ${currentTariff.max_fee_from_deal}
                            </Typography>
                            <Typography
                                sx={{
                                    fontSize: '14px',
                                    color: '#00000099',
                                    fontWeight: '500',
                                }}
                            >
                                &nbsp;мес.
                            </Typography>
                        </Box>
                    }
                    footer={
                        <Button
                            variant="contained"
                            startIcon={<CheckRoundedIcon />}
                            color="secondary"
                            sx={{ gridColumnStart: '4' }}
                        >
                            Продлить
                        </Button>
                    }
                >
                    <Wrapper>
                        <Typography>{currentTariff.descr}</Typography>
                    </Wrapper>
                </ContentWrapper>
            )}
        </Box>
    );
};

const Wrapper = styled(Box)({
    padding: '16px 24px',
});
const StyledTabs = styled((props: TabListProps) => <TabList disableRipple {...props} />)(
    ({ theme }) => ({
        '& .MuiTabs-indicator': {
            backgroundColor: theme.palette.secondary.main,
        },
    })
);

const StyledTab = styled((props: TabProps) => <Tab disableRipple {...props} />)(
    ({ theme }) => ({
        textTransform: 'none',
        fontWeight: theme.typography.fontWeightMedium,
    })
);

export default TariffPage;
