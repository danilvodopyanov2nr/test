import { lazy } from 'react';

export const TariffPageAsync = lazy(() => import('./TariffPage'));
