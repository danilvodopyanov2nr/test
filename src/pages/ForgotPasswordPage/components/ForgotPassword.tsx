import { ForgotPasswordForm } from 'pages/ForgotPasswordPage/components/ForgotPasswordForm';
import { Form } from 'pages/Login/components/Form';

const ForgotPassword = () => {
    return (
        <Form
            tooltip="Для восстановления пароля введите e-mail, указанный при регистрации."
            label="E-mail"
            form={<ForgotPasswordForm />}
        ></Form>
    );
};
export default ForgotPassword;
