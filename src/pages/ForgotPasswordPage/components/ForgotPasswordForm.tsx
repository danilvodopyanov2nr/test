import { Button, css, styled, TextField, Typography } from '@mui/material';

import { useUserStore } from 'app/store/useUserStore/useUserStore';
import { zodResolver } from '@hookform/resolvers/zod';
import { useEffect } from 'react';
import { Controller, FormProvider, useForm } from 'react-hook-form';
import { useNavigate, useLocation } from 'react-router-dom';

import { useAuth } from 'shared/lib/hooks/useAuth';

import { object, string, TypeOf } from 'zod';

import { RoutePath } from 'app/providers/router/config/routerConfig';

import { useWebSocket } from 'app/providers/WebSocket/WebSocket.hook';

interface LocationType {
    from: string;
}

const forgotPasswordSchema = object({
    login: string().min(1, 'Введите email').email('Email не подходит'),
});

export type EmailInput = TypeOf<typeof forgotPasswordSchema>;

export const ForgotPasswordForm = (): JSX.Element => {
    const { isAuth } = useAuth();

    const { user } = useUserStore((state) => state);
    const navigate = useNavigate();
    const { state } = useLocation();

    const from = (state as LocationType)?.from || '/';
    const { status, sendMessage } = useWebSocket();
    const methods = useForm({
        resolver: zodResolver(forgotPasswordSchema),
        defaultValues: {
            login: '',
        },
    });
    const {
        reset,
        control,
        handleSubmit,
        formState: { isSubmitSuccessful },
    } = methods;

    useEffect(() => {
        if (isSubmitSuccessful) {
            reset();
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isSubmitSuccessful]);

    useEffect(() => {
        if (isAuth) {
            navigate(RoutePath.main);
        }
    }, [isAuth]);

    const onSubmit = async ({ login }: EmailInput) => {
        if (status.isOpen) {
            sendMessage({
                forgot_password: {
                    session_id: user.session_id,
                    login,
                },
            });
            navigate(from);
        }
    };

    return (
        <FormProvider {...methods}>
            <form style={{ width: '100%' }} onSubmit={handleSubmit(onSubmit)}>
                <Controller
                    name="login"
                    control={control}
                    render={({ field }) => (
                        <LoginInput
                            type="email"
                            placeholder="Email"
                            fullWidth
                            {...field}
                        />
                    )}
                />
                <LoginButton
                    variant="contained"
                    color="secondary"
                    fullWidth
                    type="submit"
                >
                    Отправить
                </LoginButton>
            </form>
        </FormProvider>
    );
};

const StyledCharacter = css`
    border: none;
`;

const LoginInput = styled(TextField)(() => ({}));

const LoginButton = styled(Button)({
    marginTop: '24px',
    padding: '13px 0',
});

const LabelInput = styled(Typography)({
    marginTop: '30px',
    fontSize: '16px',
    fontWeight: '500',
});
