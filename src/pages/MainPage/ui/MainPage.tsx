import { useUserStore } from 'app/store/useUserStore/useUserStore';
import { useEffect } from 'react';
import { RoutePath } from 'app/providers/router/config/routerConfig';
import { useLocation, useNavigate } from 'react-router-dom';

import { ContentWrapper, TradingViewChart } from 'shared';

interface LocationType {
    from: string;
}

const MainPage = () => {
    const { user } = useUserStore();
    const navigate = useNavigate();
    const { state } = useLocation();
    const from = (state as LocationType)?.from || '/';

    useEffect(() => {
        console.log(user);
        if (user.type === 2) navigate(RoutePath.tariff);
    }, []);

    return (
        <>
            <ContentWrapper>
                <TradingViewChart />
            </ContentWrapper>
        </>
    );
};

export default MainPage;
