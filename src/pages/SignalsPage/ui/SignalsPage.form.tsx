import { zodResolver } from '@hookform/resolvers/zod';

import {
    Box,
    Button,
    Divider,
    MenuItem,
    Select,
    styled,
    TextField,
    TextFieldProps,
    Typography,
} from '@mui/material';
import { useWebSocket } from 'app/providers/WebSocket/WebSocket.hook';
import SaveIcon from '@mui/icons-material/Save';

import { FC, ReactNode, useEffect, useMemo } from 'react';
import { FormProvider, Controller, useForm } from 'react-hook-form';
import * as z from 'zod';
import { Signal } from 'shared/api/types';

import { useTariffsStore } from 'app/store/useTariffStore/useTariffsStore';

import { usePaymentStore } from 'app/store';

const addSignalsForm = z.object({
    name: z.string().min(1, 'Введите название'),
    type: z.string(),
    telegram_free_link: z.string(),
    descr: z.string(),
    tariffs_id: z.number(),
    wallets_id: z.array(z.number()),
});

export type AddSignalsForm = Required<z.infer<typeof addSignalsForm>>;

interface SignalsPageFormProps {
    children?: ReactNode;
    defaultValue?: AddSignalsForm & { id: number };
    handleSumbit?: (values: Signal) => void;
    removeSignal?: any;
}

export const SignalsPageForm: FC<SignalsPageFormProps> = ({
    defaultValue,
    handleSumbit,
    removeSignal,
    children,
}) => {
    const { sendMessage, status, socket } = useWebSocket();
    const { tariffs } = useTariffsStore();
    const { payment } = usePaymentStore();

    const methods = useForm({
        resolver: zodResolver(addSignalsForm),
        defaultValues: useMemo(() => {
            return (
                defaultValue || {
                    name: '',
                    type: '',
                    telegram_free_link: '',
                    descr: '',
                    tariffs_id: 0,
                    wallets_id: [],
                }
            );
        }, [defaultValue]),
    });
    const {
        reset,
        control,
        handleSubmit,
        getValues,
        formState: { defaultValues, isSubmitSuccessful, errors },
    } = methods;

    useEffect(() => {
        reset();
    }, [defaultValues]);

    useEffect(() => {
        reset(defaultValue);
    }, [defaultValue]);

    const onSubmit = () => {
        if (status.isOpen) {
            sendMessage({
                create_showcase: {
                    ...getValues(),
                    tariffs_id: [getValues().tariffs_id],
                    wallets_id: payment.wallet_types.map((el) => el.id),
                },
            });
        }

        socket.onmessage = (event: any) => {
            const data = JSON.parse(event.data.toString()) as any;

            if (data.create_showcase.done) {
                handleSumbit({
                    ...getValues(),
                    id: data.create_showcase.showcase_id,
                });
            }
        };
    };
    console.log(errors);
    return (
        <FormProvider {...methods}>
            <form style={{ width: '100%' }} onSubmit={handleSubmit(onSubmit)}>
                <Box sx={{ padding: '8px 24px' }}>
                    <Box sx={{ display: 'flex', gap: '16px' }}>
                        <Controller
                            name="name"
                            control={control}
                            render={({ field }) => (
                                <LoginInput
                                    variant="outlined"
                                    size="small"
                                    placeholder="Название"
                                    fullWidth
                                    {...field}
                                />
                            )}
                        />
                        {errors.name && (
                            <ErrorMessage>{errors.name.message}</ErrorMessage>
                        )}
                        <Controller
                            name="type"
                            control={control}
                            render={({ field }) => (
                                <LoginInput
                                    variant="outlined"
                                    size="small"
                                    placeholder="Тип"
                                    fullWidth
                                    {...field}
                                />
                            )}
                        />
                        {errors.type && (
                            <ErrorMessage>{errors.type.message}</ErrorMessage>
                        )}
                    </Box>
                    <Box sx={{ display: 'flex', gap: '16px' }}>
                        <Controller
                            name="telegram_free_link"
                            control={control}
                            render={({ field }) => (
                                <LoginInput
                                    variant="outlined"
                                    size="small"
                                    placeholder="Канал"
                                    fullWidth
                                    {...field}
                                />
                            )}
                        />
                        {errors.telegram_free_link && (
                            <ErrorMessage>
                                {errors.telegram_free_link.message}
                            </ErrorMessage>
                        )}
                        <Controller
                            name="tariffs_id"
                            control={control}
                            render={({ field }) => (
                                <Select
                                    fullWidth
                                    id="type-select"
                                    sx={{ marginTop: '16px', height: '40px' }}
                                    {...field}
                                >
                                    {tariffs.map((t) => (
                                        <MenuItem key={t.tariffs_id} value={t.tariffs_id}>
                                            {t.name}
                                        </MenuItem>
                                    ))}
                                </Select>
                            )}
                        />
                    </Box>
                    <Controller
                        name="descr"
                        control={control}
                        render={({ field }) => (
                            <LoginInput
                                variant="outlined"
                                size="small"
                                placeholder="Описание"
                                fullWidth
                                {...field}
                            />
                        )}
                    />
                    {errors.descr && <ErrorMessage>{errors.descr.message}</ErrorMessage>}
                </Box>
                {children}
                <Divider />
                <Box
                    sx={{
                        minWidth: '400px',
                        display: 'grid',
                        gridTemplateColumns: 'repeat(4, 1fr)',

                        padding: '14px 24px',
                        alignItems: 'center',
                    }}
                >
                    {defaultValue?.id && (
                        <Button
                            onClick={() => removeSignal(defaultValue.id)}
                            variant="contained"
                            startIcon={<SaveIcon />}
                            color="error"
                            sx={{ gridColumnStart: 1 }}
                        >
                            Удалить
                        </Button>
                    )}
                    <Button
                        type="submit"
                        variant="contained"
                        startIcon={<SaveIcon />}
                        color="secondary"
                        sx={{ gridColumnStart: 4 }}
                    >
                        Сохранить
                    </Button>
                </Box>
            </form>
        </FormProvider>
    );
};

const LoginInput = styled(TextField)<TextFieldProps>(() => ({
    marginTop: '16px',
    '&:last-child': {
        marginBottom: '16px',
    },
}));

const LoginButton = styled(Button)({
    marginTop: '50px',
    backgroundColor: '#ffffff',
    color: '#080710',
    padding: ' 15px 0',
    fontWeight: '600',
    '&:hover': {
        backgroundColor: '#ffffffea',
    },
});

const LabelInput = styled(Typography)({
    marginTop: '30px',
    fontSize: '16px',
    fontWeight: '500',
});

const ErrorMessage = styled(Typography)({
    color: 'red',
    fontSize: '12px',
});
