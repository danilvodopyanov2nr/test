import { lazy } from 'react';

export const SignalsPageAsync = lazy(() => import('./SignalsPage'));
