import { Box, Button, Divider, MenuItem, MenuList } from '@mui/material';
import { useEffect, useState } from 'react';

import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';

import { getSignals } from 'shared/api/api';
import { useWebSocket } from 'app/providers/WebSocket/WebSocket.hook';

import { SignalsPageForm } from 'pages/SignalsPage/ui/SignalsPage.form';
import { useUserStore } from 'app/store/useUserStore/useUserStore';
import { Signal } from 'shared/api/types';
import { PaymentForm } from 'widgets/PaymentModal';

import { WalletList } from 'pages/SignalsPage/ui/WalletList/WalletList';
import { WalletProvider } from 'features';
import { ContentWrapper } from 'shared';

const SignalsPage = () => {
    const [signals, setSignals] = useState<Signal[]>(null);
    const [currentSignal, setCurrentSignal] = useState<Signal>(null);
    const [newSignal, setNewSignal] = useState<Signal | boolean>(null);
    const { user } = useUserStore();
    const { sendMessage, socket, status } = useWebSocket();
    useEffect(() => {
        getSignals(user.access_token).then((res) => {
            setSignals(res.get_showcase.showcases);
        });
    }, []);

    useEffect(() => {
        if (status.isOpen) {
            sendMessage({ get_tariff_info: {} });
        }
    }, [status.isOpen]);
    const handleSetSignal = (id: number) => {
        const newCurrentSignal = signals.find((signal) => signal.id === id);
        setCurrentSignal(newCurrentSignal);
        setNewSignal(null);
    };
    const handleSaveSignal = (value: any) => {
        setNewSignal(null);
        setCurrentSignal(value);
        setSignals([...signals, value]);
    };

    const handleAddSignal = () => {
        setCurrentSignal(null);
        setNewSignal(true);
    };

    return (
        <Box sx={{ display: 'flex' }}>
            <ContentWrapper
                label="Сигналы"
                header={
                    <Button
                        variant="contained"
                        startIcon={<AddCircleOutlineIcon />}
                        color="secondary"
                        onClick={handleAddSignal}
                    >
                        Добавить
                    </Button>
                }
            >
                <MenuList>
                    {signals
                        ?.filter((value) => Boolean(value.name))
                        ?.map((signal) => (
                            <MenuItem
                                sx={{
                                    padding: '16px',
                                    fontWeight: '400',
                                    color:
                                        currentSignal?.id === signal.id
                                            ? '#5C6BC0'
                                            : 'inherit',
                                    borderRight:
                                        currentSignal?.id === signal.id
                                            ? '2px solid #5C6BC0;'
                                            : 'none',
                                    background:
                                        currentSignal?.id === signal.id
                                            ? '#5C6BC00A'
                                            : 'none',
                                    '&:hover': {
                                        color: '#5C6BC0',
                                        background: '#5C6BC00A',
                                    },
                                }}
                                key={signal.id}
                                onClick={() => handleSetSignal(signal.id)}
                            >
                                {signal.name}
                            </MenuItem>
                        ))}
                </MenuList>
            </ContentWrapper>
            {newSignal && (
                <WalletProvider>
                    <ContentWrapper fullWidth>
                        <SignalsPageForm handleSumbit={handleSaveSignal}>
                            <Divider />
                            <Box>
                                <PaymentForm></PaymentForm>
                            </Box>
                            <Divider />
                            <Box sx={{ padding: '8px 24px' }}>
                                <WalletList />
                            </Box>
                        </SignalsPageForm>
                    </ContentWrapper>
                </WalletProvider>
            )}
            {currentSignal && (
                <ContentWrapper fullWidth>
                    <SignalsPageForm
                        handleSumbit={handleSaveSignal}
                        defaultValue={currentSignal}
                    >
                        <Divider />
                        <Box>
                            <WalletProvider>
                                <PaymentForm></PaymentForm>
                            </WalletProvider>
                        </Box>
                        <Divider />
                        <Box sx={{ padding: '8px 24px' }}>
                            <WalletList walletListId={currentSignal.wallets_id} />
                        </Box>
                    </SignalsPageForm>
                </ContentWrapper>
            )}
        </Box>
    );
};

export default SignalsPage;
