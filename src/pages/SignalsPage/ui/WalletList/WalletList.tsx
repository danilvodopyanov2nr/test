import { useWebSocket } from 'app/providers/WebSocket/WebSocket.hook';
import { SystemWallet } from 'app/store';

import { FC, useEffect, useState } from 'react';

interface WalletListProps {
    walletListId?: number[];
}

export interface GetSystemWalletsResponse {
    get_system_wallets: {
        task: string;
        wallets: SystemWallet[];
    };
}

export const WalletList: FC<WalletListProps> = ({ walletListId }) => {
    const [walletList, setWalletList] = useState<SystemWallet[]>();
    const { sendMessage, socket, status } = useWebSocket();

    useEffect(() => {
        if (status.isOpen) {
            walletListId?.forEach((walletId) => {
                sendMessage({
                    get_system_wallets: {
                        wallet_type: walletId,
                    },
                });
            });
        }
    }, []);

    return <div>Кошельков нет</div>;
};
