import { Paper, styled } from '@mui/material';
import { RoutePath } from 'app/providers/router/config/routerConfig';
import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

import { useAuth } from 'shared/lib/hooks/useAuth';

interface LandingPageProps {
    className?: string;
}

const LandingPage: React.FC<LandingPageProps> = (props) => {
    const { className } = props;
    const navigate = useNavigate();
    const { isAuth } = useAuth();

    useEffect(() => {
        if (isAuth) navigate(RoutePath.main);
        else {
            navigate(RoutePath.login);
        }
    }, [isAuth]);

    return <div></div>;
};
const Wrapper = styled(Paper)({});
export default LandingPage;
