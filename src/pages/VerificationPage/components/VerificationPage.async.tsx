import { lazy } from 'react';

export const VerificationAsync = lazy(() => import('./Verification'));
