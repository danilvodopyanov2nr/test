import { Button } from '@mui/material';
import { Form } from 'pages/Login/components/Form';

import { useVerification } from 'pages/VerificationPage/components/hook/useVerification';
import { VerificationForm } from 'pages/VerificationPage/components/VerificationForm';

const Verification = () => {
    const { resendCode } = useVerification();
    return (
        <Form
            label="Код"
            form={<VerificationForm />}
            tooltip="На Ваш почтовый ящик, указанный при регистрации был отправлен код. Введите его."
            footer={<Button onClick={() => resendCode()}>Выслать снова</Button>}
        ></Form>
    );
};
export default Verification;
