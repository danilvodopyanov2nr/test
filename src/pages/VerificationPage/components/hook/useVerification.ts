import { useUserStore } from '../../../../app/store/useUserStore/useUserStore';
import { useMutation } from '@tanstack/react-query';

import { registrationUser } from 'shared/api/api';

import { PostRegistrationUser } from 'shared/api/types';

export const useVerification = () => {
    const { registrationUserData } = useUserStore();

    const { mutate: resendCode, isLoading } = useMutation(
        () => registrationUser(registrationUserData as PostRegistrationUser),
        {
            onSuccess: (userResponse) => {
                if (userResponse.registration.needCode) {
                }
            },
        }
    );

    return {
        resendCode,
        isLoading,
    };
};
