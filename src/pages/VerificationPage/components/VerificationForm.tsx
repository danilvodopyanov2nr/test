import { css, InputBase, InputBaseProps, styled, Typography } from '@mui/material';

import { useUserStore } from 'app/store/useUserStore/useUserStore';
import { zodResolver } from '@hookform/resolvers/zod';
import { useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { useNavigate, useLocation } from 'react-router-dom';

import { useAuth } from 'shared/lib/hooks/useAuth';

import { object, string, TypeOf } from 'zod';
import { sendEmailCode } from 'shared/api/api';
import { toast } from 'react-toastify';
import { RoutePath } from 'app/providers/router/config/routerConfig';
import VerificationInput from 'react-verification-input';
import { useTranslation } from 'react-i18next';
import { Button } from 'shared';

interface LocationType {
    from: string;
}

const emailVerificationSchema = object({
    emailCode: string().min(1, 'Email code is required'),
});

export type EmailInput = TypeOf<typeof emailVerificationSchema>;

export const VerificationForm = (): JSX.Element => {
    const { isAuth } = useAuth();
    const { user, addUser, setRequestLoading } = useUserStore((state) => state);
    const navigate = useNavigate();
    const { state } = useLocation();
    const { t } = useTranslation();
    const from = (state as LocationType)?.from || '/';
    const {
        reset,
        control,
        handleSubmit,
        formState: { isSubmitSuccessful },
    } = useForm({
        resolver: zodResolver(emailVerificationSchema),
        defaultValues: {
            emailCode: '',
        },
    });

    useEffect(() => {
        if (isSubmitSuccessful) {
            reset();
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isSubmitSuccessful]);

    const checkEmailCode = async (code: string) => {
        try {
            setRequestLoading(true);
            sendEmailCode({ code, session_id: user.session_id }).then((res) => {
                if ('done' in res.email_code) {
                    toast.success('Код верный', {
                        position: 'top-right',
                    });
                    navigate(RoutePath.login);
                    setRequestLoading(false);
                } else if ('error' in res.email_code) {
                    setRequestLoading(false);
                    const message = t(res.email_code.error);
                    toast.error(message, {
                        position: 'top-right',
                    });
                }
            });
        } catch (error) {
            console.log(error);
            setRequestLoading(false);
            toast.error(error.response, {
                position: 'top-right',
            });
        }
    };

    useEffect(() => {
        if (isAuth) {
            navigate(RoutePath.main);
        }
    }, [isAuth]);

    return (
        <VerificationInput
            placeholder=""
            classNames={{
                container: 'verification__container',
                character: 'verification__character',
                characterInactive: 'verification-character__inactive',
                characterSelected: 'verification-character__selected',
            }}
            onComplete={(code) => checkEmailCode(code)}
        />
    );
};

const StyledCharacter = css`
    border: none;
`;

const LoginInput = styled(InputBase)<InputBaseProps>(() => ({
    '& .MuiInputBase-input': {
        backgroundColor: 'rgba(255,255,255,0.07)',
        borderRadius: '3px',
        padding: '0 10px',
        height: '50px',
        width: '100%',
        marginTop: '8px',
    },
}));

const LoginButton = styled(Button)({
    marginTop: '50px',
    backgroundColor: '#ffffff',
    color: '#080710',
    padding: ' 15px 0',
    fontWeight: '600',
    '&:hover': {
        backgroundColor: '#ffffffea',
    },
});

const LabelInput = styled(Typography)({
    marginTop: '30px',
    fontSize: '16px',
    fontWeight: '500',
});
