import { styled, TextField, Typography } from '@mui/material';

import { useUserStore } from 'app/store/useUserStore/useUserStore';
import LoadingButton from '@mui/lab/LoadingButton';
import { useEffect } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { useNavigate, useLocation } from 'react-router-dom';

import { useAuth } from 'shared/lib/hooks/useAuth';

import { RoutePath } from 'app/providers/router/config/routerConfig';

interface FormFields {
    login: string;
    password: string;
}

interface LocationType {
    from: string;
}

export const LoginForm = (): JSX.Element => {
    const { loginUserQuery, isAuth, user, isLoadingLogin } = useAuth();
    const addUser = useUserStore((state) => state.addUser);

    const navigate = useNavigate();
    const { state } = useLocation();
    const from = (state as LocationType)?.from || '/';

    const { control, handleSubmit } = useForm({
        defaultValues: {
            login: '',
            password: '',
        },
    });

    const onSubmit = async ({ login, password }: FormFields) => {
        loginUserQuery({
            login,
            password,
        });
    };

    useEffect(() => {}, [user]);

    useEffect(() => {
        if (isAuth) {
            addUser(user);
            navigate(RoutePath.main);
        }
    }, [isAuth]);

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <Controller
                name="login"
                control={control}
                render={({ field }) => (
                    <LoginInput
                        type="email"
                        label="Email"
                        fullWidth
                        variant="outlined"
                        {...field}
                    />
                )}
            />

            <Controller
                name="password"
                control={control}
                render={({ field }) => (
                    <LoginInput
                        variant="outlined"
                        type="password"
                        label="Пароль"
                        fullWidth
                        {...field}
                    />
                )}
            />

            <LoginButton
                loading={isLoadingLogin}
                loadingPosition="start"
                variant="contained"
                color="secondary"
                fullWidth
                type="submit"
            >
                Вход
            </LoginButton>
        </form>
    );
};

const LoginInput = styled(TextField)(() => ({
    marginBottom: 16,
}));

const LoginButton = styled(LoadingButton)({
    padding: '13px 0',
});

const LabelInput = styled(Typography)({
    marginTop: '30px',
    fontSize: '16px',
    fontWeight: '500',
});
