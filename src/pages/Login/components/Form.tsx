import { Box, Typography, Divider, styled } from '@mui/material';

import { FC, PropsWithChildren, ReactElement } from 'react';

import LogoIcon from 'shared/assets/icons/logo.svg';

interface FormProps {
    label: string;
    form?: ReactElement;
    tooltip?: string;
    footer?: ReactElement;
}

export const Form: FC<PropsWithChildren<FormProps>> = ({
    label,
    form,
    tooltip,
    children,
    footer,
}) => {
    return (
        <Box
            sx={{
                width: '100%',
                height: '100%',
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
            }}
        >
            <Box
                sx={{
                    width: '425px',
                    borderRadius: '8px',
                    backgroundColor: '#fff',
                }}
            >
                <Wrapper sx={{ padding: '24px' }}>
                    <FlexForm>
                        <LogoIcon />
                        <Typography sx={{ fontSize: '24px', fontWeight: '400' }}>
                            {label}
                        </Typography>
                    </FlexForm>
                </Wrapper>
                <Divider />
                <Wrapper>
                    {tooltip && (
                        <Typography
                            sx={{
                                color: '#9E9E9E',
                                textAlign: 'center',
                                marginBottom: '24px',
                            }}
                        >
                            {tooltip}
                        </Typography>
                    )}
                    {form}
                    {children}
                    <Box sx={{ marginTop: '16px' }}>
                        <FlexForm>{footer}</FlexForm>
                    </Box>
                </Wrapper>
            </Box>
        </Box>
    );
};

const Wrapper = styled(Box)({
    padding: '24px',
});

const FlexForm = styled(Box)({
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
});
