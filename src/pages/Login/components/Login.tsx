import { styled } from '@mui/material';

import { RoutePath } from 'app/providers/router/config/routerConfig';
import { Form } from 'pages/Login/components/Form';
import { LoginForm } from 'pages/Login/components/LoginForm';
import { LinkProps } from 'react-router-dom';
import { AppLink } from 'shared';

const Login = () => {
    return (
        <Form
            label="Вход"
            form={<LoginForm />}
            footer={
                <>
                    <StyledLink to={RoutePath.registration}>Регистрация</StyledLink>
                    <StyledLink to={RoutePath['forgot-password']}>
                        Забыли пароль?
                    </StyledLink>
                </>
            }
        ></Form>
    );
};

const StyledLink = styled((props: LinkProps) => <AppLink {...props} />)(({ theme }) => ({
    textTransform: 'none',
    color: theme.palette.secondary.main,
    '&:hover': {
        textDecoration: 'underline',
        textDecorationSkipInk: 'none',
    },
}));

export default Login;
