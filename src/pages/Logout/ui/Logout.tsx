import { useEffect } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';

import { useAuth } from 'shared/lib/hooks/useAuth';

interface LocationType {
    from: string;
}

const Logout: React.FC = (props) => {
    const { logout, isAuth } = useAuth();

    const navigate = useNavigate();
    const { state } = useLocation();
    const from = (state as LocationType)?.from || '/';

    useEffect(() => {
        logout();
    }, []);

    useEffect(() => {
        navigate(from);
    }, [isAuth]);

    return <div></div>;
};
export default Logout;
