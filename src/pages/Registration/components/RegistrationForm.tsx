import { styled, TextField, Typography } from '@mui/material';

import { zodResolver } from '@hookform/resolvers/zod';
import { useEffect } from 'react';
import { Controller, FormProvider, useForm } from 'react-hook-form';
import { useNavigate } from 'react-router-dom';

import { object, string, TypeOf } from 'zod';

import { toast } from 'react-toastify';
import { useRegistration } from 'pages/Registration/hooks/useRegistration';
import { RoutePath } from 'app/providers/router/config/routerConfig';
import { LoadingButton } from '@mui/lab';

const registerSchema = object({
    login: string().min(1, 'Введите email').email('Email не подходит'),
    password: string()
        .min(1, 'Введите пароль')
        .min(8, 'Пароль должен быть больше 8 символов')
        .max(32, 'Password must be less than 32 characters'),
    passwordConfirm: string().min(1, 'Введите пароль'),
}).refine((data) => data.password === data.passwordConfirm, {
    path: ['passwordConfirm'],
    message: 'Пароли должны совпадать',
});

export type RegisterInput = TypeOf<typeof registerSchema>;

export const RegistrationForm = (): JSX.Element => {
    const navigate = useNavigate();

    const methods = useForm({
        resolver: zodResolver(registerSchema),
        defaultValues: {
            login: '',
            password: '',
            passwordConfirm: '',
        },
    });
    const {
        reset,
        control,
        handleSubmit,
        formState: { isSubmitSuccessful, errors },
    } = methods;

    useEffect(() => {
        if (isSubmitSuccessful) {
            reset();
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isSubmitSuccessful]);

    const { registerUser, isSuccess, isError, isLoading } = useRegistration();

    const onSubmit = async ({ login, password }: RegisterInput) => {
        registerUser({
            login,
            password,
        });
    };

    useEffect(() => {
        if (isSuccess) {
            toast.success('Пользователь создан', {
                position: 'top-right',
            });
            navigate(RoutePath.verification);
        }
        if (isError) {
            toast.error('Ошибка создания пользователя, попробуйте позже', {
                position: 'top-right',
            });
        }
    }, [isSuccess, isError]);

    return (
        <FormProvider {...methods}>
            <form style={{ width: '100%' }} onSubmit={handleSubmit(onSubmit)}>
                <Controller
                    name="login"
                    control={control}
                    render={({ field }) => (
                        <LoginInput
                            type="email"
                            placeholder="Email"
                            fullWidth
                            {...field}
                        />
                    )}
                />
                {errors.login && <ErrorMessage>{errors.login.message}</ErrorMessage>}

                <Controller
                    name="password"
                    control={control}
                    render={({ field }) => (
                        <LoginInput
                            type="password"
                            placeholder="Пароль"
                            fullWidth
                            {...field}
                        />
                    )}
                />
                {errors.password && (
                    <ErrorMessage>{errors.password.message}</ErrorMessage>
                )}

                <Controller
                    name="passwordConfirm"
                    control={control}
                    render={({ field }) => (
                        <LoginInput
                            type="password"
                            placeholder="Повторите пароль"
                            fullWidth
                            {...field}
                        />
                    )}
                />
                {errors.passwordConfirm && (
                    <ErrorMessage>{errors.passwordConfirm.message}</ErrorMessage>
                )}
                <LoginButton
                    loading={isLoading}
                    loadingPosition="start"
                    variant="contained"
                    color="secondary"
                    fullWidth
                    type="submit"
                >
                    Зарегистрироваться
                </LoginButton>
            </form>
        </FormProvider>
    );
};

const LoginInput = styled(TextField)(() => ({
    marginBottom: 16,
}));

const LoginButton = styled(LoadingButton)({
    padding: '13px 0',
});

const ErrorMessage = styled(Typography)({
    color: 'red',
    fontSize: '12px',
});
