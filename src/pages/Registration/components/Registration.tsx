import { styled } from '@mui/material';
import { RoutePath } from 'app/providers/router/config/routerConfig';
import { Form } from 'pages/Login/components/Form';

import { RegistrationForm } from 'pages/Registration/components/RegistrationForm';
import { LinkProps } from 'react-router-dom';
import { AppLink } from 'shared';

const Registration = () => {
    return (
        <Form
            label="Регистрация"
            footer={<StyledLink to={RoutePath.login}>Войти</StyledLink>}
        >
            <RegistrationForm />
        </Form>
    );
};

const StyledLink = styled((props: LinkProps) => <AppLink {...props} />)(({ theme }) => ({
    textTransform: 'none',
    color: theme.palette.secondary.main,
    marginTop: '16px',

    '&:hover': {
        textDecoration: 'underline',
        textDecorationSkipInk: 'none',
    },
}));

export default Registration;
