import { useUserStore } from '../../../app/store/useUserStore/useUserStore';
import { registrationUser } from './../../../shared/api/api';
import { useMutation } from '@tanstack/react-query';

import { LoginInput } from 'shared/types/inputTypes';
import { useRef } from 'react';

export const useRegistration = () => {
    const { user, addRegistrationUserData } = useUserStore();
    const registrationData = useRef(null);
    const language_id = 1;

    const {
        mutate: registerUser,
        isLoading,
        isError,

        isSuccess,
    } = useMutation(
        (userData: LoginInput) => {
            addRegistrationUserData({
                ...userData,
                session_id: user.session_id,
                language_id,
            });
            return registrationUser({
                ...userData,
                session_id: user.session_id,
                language_id,
            });
        },
        {
            onSuccess: (userResponse) => {
                if (userResponse.registration.needCode) {
                }
            },
        }
    );
    return {
        registerUser,
        registrationData: registrationData.current,
        isLoading,
        isSuccess,
        isError,
    };
};
