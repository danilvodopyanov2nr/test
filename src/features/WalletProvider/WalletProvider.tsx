import { useWebSocket } from 'app/providers/WebSocket/WebSocket.hook';
import { Wallet, SystemWallet } from 'app/store';

import {
    Context,
    FC,
    PropsWithChildren,
    createContext,
    useEffect,
    useState,
} from 'react';

export interface IWalletContext {
    setWalletId: (id: number) => void;
}

export interface GetWalletsTypesResponse {
    get_wallet_types: {
        task: string;
        wallet_types: Wallet[];
    };
}

export interface GetSystemWalletsResponse {
    get_system_wallets: {
        task: string;
        wallets: SystemWallet[];
    };
}

export const WalletContext: Context<Partial<IWalletContext>> = createContext({});

export const WalletProvider: FC<PropsWithChildren> = ({ children }) => {
    const [walletId, setWalletId] = useState(null);

    const { socket, sendMessage, status } = useWebSocket();

    useEffect(() => {
        if (status.isOpen) {
            sendMessage({
                get_wallet_types: {},
            });
        }
    }, []);

    useEffect(() => {
        if (walletId) {
            sendMessage({
                get_system_wallets: {
                    wallet_type: walletId,
                },
            });
        }
    }, [walletId]);

    const value: IWalletContext = {
        setWalletId,
    };

    return <WalletContext.Provider value={value}>{children}</WalletContext.Provider>;
};
