import { WalletContext } from 'features/WalletProvider/WalletProvider';
import { useContext } from 'react';

export const useWalletProvider = () => {
    const context = useContext(WalletContext);
    if (!context) {
        throw new Error('useWebsocket must be used within a WalletContext');
    }
    return context;
};
